# Incremental Trajectory Stream Anonymizer (ITSA)

The ITSA algorithm aims to anonymize a stream of trajectories, under the LKC privacy principle.
The basic idea is to use a Sliding Window approach to compute small batches of data, and
incrementally anonymize the trajectory stream via suppression. It considers a Sliding Window
based on the temporal dimension, wich slides with a certain step size, which must be smaller than
the actual window size, to ensure the success of the algorithm.

## Input dataset

The project is designed to accept three different dataset as input sources, which are:

* _cariploenr6_ AND _cariploenr6-sample-1min_ are table accessible on Hive, which have been used to test the algorithm. It must have at least the following columns:
  * _customid:String_ contains a custom ID for every point of a trajectory
  * _trajid:String_ contains the trajectory ID for every point of a trajectory
  * _latitude:Double_ contains the latitude of a point
  * _longitude:Double_ contains the longitude of a point
  * _timest:Long_ contains seconds elapsed since 1/1/1970(unix_time) as a Long
* _final-oldenburg-dataset_ is an Hive table containing trajectories from the Oldenburg dataset. It must have at least the following columns:
  * _customid:String_ contains a custom ID for every point of a trajectory
  * _trajid:String_ contains the trajectory ID for every point of a trajectory
  * _latitude:Double_ contains the latitude of a point
  * _longitude:Double_ contains the longitude of a point
  * _timestamp:Long_ contains an integer value representing an instant in time
* _MetroDataSet_ is a text file containing trajectories from Metro Data, it is the main dataset against which the code has been tested. It must have the following fashion:
  * Locations represented by the letter _L_ followed by an integer number
  * Time represented by the letter _T_ followed by an integer value, representing an instant.
  * Each line of the text file represent a different trajectory, and must be written in the form of:
  _Lx,Ty,......,LtTz_:_Attribute_. Attribute is a value of an attribute for the trajectory.
  * The text file must be paired with a _.cfg_ file containing the definitions of the sensitive values for the attributes listed in the dataset

## Output results

Output results of the algorithm are written into some CSV files into an _output/_ folder, placed in the folder
where the program gets launched. The algorithm generates potentially two _.csv_ files, that are:
* _ITSA-DATASET-param1_value1-param2_value2[...].csv_ : file generated with any dataset passed as an input, and contains all the output anonymized dataset, under csv form, with the following header: 
  * _userid_; _trajectoryid_; _latitude_; _longitude_; _timestamp_
* _ITSA-ATTRIBUTES-param1_value1-param2_value2[...].csv_ : file generated only having metro-like datasets as input, in fact those are the only datasets containing sensitive attributes values. This file contains attributes and respective values associated to each user and trajectory id and is saved under csv form, with the following header:
  * _userid_; _trajectoryid_; _attribute_; _value_

The file names contain various input/output parameters associated with their values:
* _L_ : is the specified value for the L parameter of the LKC privacy principle.
* _K_ : is the specified value for the K parameter of the LKC privacy principle.
* _C_ : is the specified value for the C parameter of the LKC privacy principle.
* _N_ : is the temporal size of the sliding window, in minutes.
* _stepSize_ : is the temporal step size of the sliding window, in minutes. Must be smaller than N.
* _dataset_ : is the name of the specified input dataset.
* _rows_ : is the number of trajectories considered from the dataset.
* _distRatio_ : is the output distortion ratio, measuring data distortion produced by the algorithm.

## How to run this project

A gradle task is available: _itsaJar_ which build a fat jar
based on _Spark 2.11_ to execute the **ITSA** algorithm, both with Hive tables as input dataset and
MetroData-like text files.

Tests are available via the `--debug` option on launch.

Simply run the following command:

```
 spark-submit --class Main /path/to/jar/CentralizedITSA-1.0.0-spark.jar  --threshold_l=3 \
  --threshold_k=20 \
  --threshold_c=0.4 \
  --window_size_n=7 \
  --step_size=1 \
  --dataset=final_oldenburg_dataset \
  --rounding=1 \
  --rounding_olden=1000 \
  --max_time_olden=60 \
  --debug \
  --num_rows=10000 \
  --sampling=1 \
  --exec_memory=5 \
  --executors=5 \
  --driver_memory=5

```

where:

* `--threshold_l=L` is the specified value for the L parameter of the LKC privacy principle.
* `--threshold_k=K` is the specified value for the K parameter of the LKC privacy principle.
* `--threshold_c=C` is the specified value for the C parameter of the LKC privacy principle.
* `--window_size_n=N` is the temporal size of the sliding window, in minutes.
* `--step_size=1` is the temporal step size of the sliding window, in minutes. Must be smaller than N.
* `--dataset=dataset_name` is the name of the specified input dataset.
* `--rounding=integer` is the number of decimal ciphers that must be considered into the spatial coordinates of the trajectories' points. _Optional value_
* `--rounding_olden=integer` is the number that further rounds the spatial coordinates, designed for the oldenburg table. _Optional value_
* `--max_time_olden=integer` is the max timestamp in the Oldenburg dataset to consider. _Optional value_
* `--num_rows=large_integer` is the number of trajectories considered from the dataset. _Optional value_
* `--sampling=1` is the sampling value for the temporal information of the trajectories' points: if the value is 10, the temporal values are sampled every 10 minutes. _Optional value_
* `--debug`  boolean value, if true prints more information on the output console. _Optional value_
* `--exec_memory=integer` is the number of GBs to allocate for each Spark executor; if not specified is 5 by default. _Optional value_
* `--executors=integer` is the number of executors on the Spark framework; if not specified is 5 by default. _Optional value_
* `--driver_memory=integer` is the number of memory GBs to be given to the Spark Driver; if not specified is 5 by default. _Optional value_ 
* `--kryo_max=integer` is the number of max MBs of Kryo buffer; if not specified is 512 by default. _Optional value_ 

## Project Resources

Text files are present into the `resources/dataset/metrolike` folder, divided between actual datasets, and their respective configuration file.
* Text files saved into the `resources/dataset/metrolike/text` folder: these files are written in a MetroData fashion, and have been mainly used for test purposes, some of them are used into the actual code tests, some others have been useful throughout the implementation to test small bunches of data.
* Configuration files saved into the `resources/dataset/metrolike/config` folder: these files match the syntax of their respective used by an executable file provided by the ITSA authors, and are used by the project basically to retrieve all the possible attributes and their sensitive values.
import itsa.ImprovedITSA
import model.{Dataset, Doublet, WindowRecords}
import org.scalatest.funsuite.AnyFunSuite
import parser.TextParser
import utils.StringUtils

import scala.collection.mutable

/**
 * This test unit simply tests the results produced by the ITSA algorithm.
 * It only needs a Window, a dataset name and the ITSA parameters.
 */
class ItsaTest extends AnyFunSuite{

  final val datasetName = StringUtils.METRO_DATASET
  val newWindow2 = new WindowRecords(mutable.Map.empty, 7, 1, Option.empty)
  val sensitiveValuesMy: Map[String, List[String]] = TextParser.getSensitiveValuesFromConfig(datasetName)
  val datasetAndTotal: (Dataset, BigInt) = TextParser.textFileToMapOfRecords(datasetName, None)


  val timeStart: Long = System.nanoTime
  val windowsPublishedImproved: List[Map[Doublet, Set[String]]] = ImprovedITSA.doITSA(newWindow2,
    datasetAndTotal._1, sensitiveValuesMy, 2, 40, 0.6, datasetAndTotal._2, debug = true)
  val duration: Double = (System.nanoTime - timeStart) / 1e9d

  test("New Dataset Test with improved ITSA") {
    println(s"Total doublets: ${datasetAndTotal._2}")
    println(s"There are ${windowsPublishedImproved.size} windows")
    println(s"total doublets suppressed: ${ImprovedITSA.totalSuppressed}")
    println(s"Execution took $duration seconds")
    println(s"\ndistortion ratio: ${ImprovedITSA.distortionRatio}%")
    for((_,i) <- windowsPublishedImproved.view.zipWithIndex) {
      println(s"--------Window ${i + 1}--------")
      val totalDoubletsInWindow = windowsPublishedImproved(i).values.map(_.size).toList.sum
      println(totalDoubletsInWindow)
    }
  }

}

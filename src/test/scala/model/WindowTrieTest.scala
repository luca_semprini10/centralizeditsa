package model

import model.windowtrie.TrieNode
import org.scalatest.funsuite.AnyFunSuite
import parser.TextParser

import scala.collection.mutable

/**
 * Test unit used to test various features of a Trie (prefix tree): firstly, append function is tested, then
 * the two counting methods, to count subsequences inside the tree and count subsequences having sensitive
 * attributes. Also empty function is tested.
 */
class WindowTrieTest extends AnyFunSuite {
  var newWindow = new WindowRecords(mutable.Map.empty, 3, 1, Option.empty)
  val windowTrie = new TrieNode()
  var newWindowMy = new WindowRecords(mutable.Map.empty, 7, 1, Option.empty)
  val windowTrieMy = new TrieNode()
  val format = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
  val dataset: List[DatasetRecord] = List(
    new DatasetRecord(1, List(
      new Doublet(
        1.toString,
        2.2,
        2.2,
        format.parse("2020-04-11 13:00:00").getTime
      ),
      new Doublet(
        1.toString,
        3.3,
        3.3,
        format.parse("2020-04-11 14:00:00").getTime
      ),
      new Doublet(
        1.toString,
        4.4,
        4.4,
    format.parse("2020-04-11 15:00:00").getTime
  )
    ), Map("1" -> "s1")),
    new DatasetRecord(2, List(
      new Doublet(
        2.toString,
        1.1,
        1.1,
        format.parse("2020-04-11 12:00:00").getTime
      ),
      new Doublet(
        2.toString,
        2.3,
        2.3,
        format.parse("2020-04-11 13:00:00").getTime
      ),
      new Doublet(
        2.toString,
        3.3,
        3.3,
        format.parse("2020-04-11 14:00:00").getTime
      ),
      new Doublet(
        2.toString,
        4.4,
        4.4,
        format.parse("2020-04-11 15:00:00").getTime
      )
    ), Map("1" -> "s2")),
    new DatasetRecord(3, List(
      new Doublet(
        3.toString,
        2.2,
        2.2,
        format.parse("2020-04-11 13:00:00").getTime
      ),
      new Doublet(
        3.toString,
        3.3,
        3.3,
        format.parse("2020-04-11 14:00:00").getTime
      ),
      new Doublet(
        3.toString,
        4.4,
        4.4,
        format.parse("2020-04-11 15:00:00").getTime
      )
    ), Map("1" -> "s3")),
    new DatasetRecord(4, List(
      new Doublet(
        4.toString,
        1.1,
        1.1,
        format.parse("2020-04-11 12:00:00").getTime
      ),
      new Doublet(
        4.toString,
        2.3,
        2.3,
        format.parse("2020-04-11 13:00:00").getTime
      ),
      new Doublet(
        4.toString,
        3.3,
        3.3,
        format.parse("2020-04-11 14:00:00").getTime
      )
    ), Map("1" -> "s4")),
    new DatasetRecord(5, List(
      new Doublet(
        5.toString,
        2.2,
        2.2,
        format.parse("2020-04-11 13:00:00").getTime
      ),
      new Doublet(
        5.toString,
        3.3,
        3.3,
        format.parse("2020-04-11 14:00:00").getTime
      )
    ), Map("1" -> "s5")),
    new DatasetRecord(6, List(
      new Doublet(
        6.toString,
        3.3,
        3.3,
        format.parse("2020-04-11 14:00:00").getTime
      )
    ), Map("1" -> "s2")),
    new DatasetRecord(7, List(
      new Doublet(
        7.toString,
        2.3,
        2.3,
        format.parse("2020-04-11 13:00:00").getTime
      ),
      new Doublet(
        7.toString,
        4.4,
        4.4,
        format.parse("2020-04-11 15:00:00").getTime
      )
    ), Map("1" -> "s3")),
    new DatasetRecord(8, List(
      new Doublet(
        8.toString,
        3.3,
        3.3,
        format.parse("2020-04-11 14:00:00").getTime
      ),
      new Doublet(
        8.toString,
        4.5,
        4.5,
        format.parse("2020-04-11 15:00:00").getTime
      )
    ), Map("1" -> "s1"))
  )
  val doubletDataset: List[Doublet] = dataset.flatten(r => r.trajectory).sortBy(doublet => doublet.timestamp)

  val datasetRecords: List[DatasetRecord] = TextParser.textFileToListOfRecords("paperDataset")
  val map: Dataset = Dataset(datasetRecords
    .map(record => record.personId.toString -> (record.trajectory, record.senAtt)).toMap)

  newWindow.initializeWindow(map, map.dataset.values.flatMap(_._1).minBy(d => d.timestamp).timestamp,
    map.dataset.values.flatMap(_._1).maxBy(d => d.timestamp).timestamp)
  windowTrie.append(newWindow.windowRecords.toMap)

  val countC3: Int = windowTrie.countSubsequence(map.dataset(5.toString)._1)
  val countA1C3: Int = windowTrie.countSubsequence(List(map.dataset(1.toString)._1.head, map.dataset(1.toString)._1(2)))
  val countA1B2: Int = windowTrie.countSubsequence(List(map.dataset(1.toString)._1.head, map.dataset(0.toString)._1.head))
  val countB2: Int = windowTrie.countSubsequence(List(map.dataset(0.toString)._1.head))

  val confidenceForB2C3: Map[String, Double] = windowTrie.countSubsequenceWithAttribute(
    List(map.dataset(0.toString)._1.head, map.dataset(0.toString)._1(1)),
    3,
    TextParser.getSensitiveValuesFromConfig("paperDataset").values.toList.flatten)
  val confidenceForC3: Map[String, Double] = windowTrie.countSubsequenceWithAttribute(
    map.dataset(5.toString)._1,
    countC3,
    TextParser.getSensitiveValuesFromConfig("paperDataset").values.toList.flatten)

  test("Check new trie methods") {
    assert(countC3 == 7)
    assert(countB2 == 3)
    assert(countA1C3 == 2)
    assert(countA1B2 == 0)
    assert(confidenceForC3("HIV") == 2.toDouble / 7.toDouble)
    assert(confidenceForB2C3("HIV") == 1.toDouble / 3.toDouble)
  }

  val trieToEmpty: TrieNode = windowTrie
  trieToEmpty.empty()
  test("Check trie clearing") {
    assert(trieToEmpty.ids.isEmpty)
    assert(trieToEmpty.attributes.isEmpty)
    assert(trieToEmpty.children.isEmpty)
    assert(trieToEmpty.rootNode == 0)
  }

  val myDatasetRecords: List[DatasetRecord] = TextParser.textFileToListOfRecords("myDataset")
  val sensitiveValuesMy: Map[String, List[String]] = TextParser.getSensitiveValuesFromConfig("myDataset")
  val myMap: Dataset = Dataset(myDatasetRecords
    .map(record => record.personId.toString -> (record.trajectory, record.senAtt)).toMap)

  newWindowMy.initializeWindow(myMap, myMap.dataset.values.flatMap(_._1).minBy(d => d.timestamp).timestamp,
    myMap.dataset.values.flatMap(_._1).maxBy(d => d.timestamp).timestamp)
  windowTrieMy.append(newWindowMy.windowRecords.toMap)
  val countB2M7: Int = windowTrieMy.countSubsequence(List(myMap.dataset(0.toString)._1(1),
    myMap.dataset(1.toString)._1(5)))
  val countB2C3: Int = windowTrieMy.countSubsequence(List(myMap.dataset(0.toString)._1(1),
    myMap.dataset(1.toString)._1(1)))
  val countB2C3D4: Int = windowTrieMy.countSubsequence(List(myMap.dataset(0.toString)._1(1),
    myMap.dataset(1.toString)._1(1), myMap.dataset(1.toString)._1(2)))
  val countC3L5: Int = windowTrieMy.countSubsequence(List(myMap.dataset(4.toString)._1.head,
    myMap.dataset(1.toString)._1(3)))
  val countC3L5G7: Int = windowTrieMy.countSubsequence(List(myMap.dataset(4.toString)._1.head,
    myMap.dataset(1.toString)._1(3), myMap.dataset(0.toString)._1(6)))

  test("Check new trie methods with my dataset") {
    assert(countB2M7 == 1)
    assert(countB2C3 == 2)
    assert(countB2C3D4 == 2)
    assert(countC3L5 == 2)
    assert(countC3L5G7 == 0)
  }

  val trieToEmptyMy: TrieNode = windowTrieMy
  trieToEmptyMy.empty()
  test("Check second trie clearing") {
    assert(trieToEmpty.ids.isEmpty)
    assert(trieToEmpty.attributes.isEmpty)
    assert(trieToEmpty.children.isEmpty)
    assert(trieToEmpty.rootNode == 0)
  }
}

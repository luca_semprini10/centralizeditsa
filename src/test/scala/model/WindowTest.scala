package model

import scala.collection.mutable
import org.scalatest.funsuite.AnyFunSuite
import parser.TextParser

/**
 * Test unit used to test the various features of a sliding Window: it tests window initialization
 * and window sliding, checking results for every single slide with multiple datasets.
 */
class WindowTest extends AnyFunSuite {
  var newWindow = new WindowRecords(mutable.Map.empty, 3, 1, Option.empty)
  var newWindow2 = new WindowRecords(mutable.Map.empty, 7, 1, Option.empty)

  val listOfDoublets: List[Doublet] = TextParser.textFileToListOfRecords("metro")
    .flatten(r => r.trajectory)
    .sortBy(doublet => doublet.timestamp)

  val datasetRecords: List[DatasetRecord] = TextParser.textFileToListOfRecords("paperDataset")
  val map: Dataset = Dataset(datasetRecords
    .map(record => record.personId.toString -> (record.trajectory, record.senAtt)).toMap)

  newWindow.initializeWindow(map, map.dataset.values.flatMap(_._1).minBy(d => d.timestamp).timestamp,
    map.dataset.values.flatMap(_._1).maxBy(d => d.timestamp).timestamp)
  test("Window with dataset records") {
    assert(newWindow.windowRecords.values.count(_._1.nonEmpty) == 8)
  }

  newWindow.slideWindow(map)
  test("Window slided with dataset records") {
    assert(newWindow.mapDoublets.values.size == 5)
    assert(newWindow.windowRecords.values.count(_._1.nonEmpty) == 8)
  }

  val myDatasetRecords: List[DatasetRecord] = TextParser.textFileToListOfRecords("myDataset")
  val myMap: Dataset = Dataset(myDatasetRecords
    .map(record => record.personId.toString -> (record.trajectory, record.senAtt)).toMap)

  newWindow2.initializeWindow(myMap, myMap.dataset.values.flatMap(_._1).minBy(d => d.timestamp).timestamp,
    map.dataset.values.flatMap(_._1).maxBy(d => d.timestamp).timestamp)
  test("Window initialized on my Dataset") {
    assert(newWindow2.windowRecords.values.map(_._1.size).sum == 36)
  }

  var newWindow2slided = new WindowRecords(mutable.Map.empty, 7, 1, Option.empty)
  newWindow2slided.initializeWindow(myMap, myMap.dataset.values.flatMap(_._1).minBy(d => d.timestamp).timestamp,
    map.dataset.values.flatMap(_._1).maxBy(d => d.timestamp).timestamp)
  newWindow2slided.slideWindow(myMap)
  test("Window slided on my Dataset") {
    assert(newWindow2slided.windowRecords.values.map(_._1.size).sum == 39)
  }

  var newWindow2slidedSecondTime = new WindowRecords(mutable.Map.empty, 7, 1, Option.empty)
  newWindow2slidedSecondTime.initializeWindow(myMap, myMap.dataset.values.flatMap(_._1).minBy(d => d.timestamp).timestamp,
    map.dataset.values.flatMap(_._1).maxBy(d => d.timestamp).timestamp)
  newWindow2slidedSecondTime.slideWindow(myMap)
  newWindow2slidedSecondTime.slideWindow(myMap)
  test("Window slided on my Dataset second time") {
    assert(newWindow2slidedSecondTime.windowRecords.values.map(_._1.size).sum == 37)
  }

}

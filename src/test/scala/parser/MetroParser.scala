package parser

import model.DatasetRecord
import org.scalatest.funsuite.AnyFunSuite

/**
 * Test unit to check the method in MetroParser Object, to parse text files in MetroData style into
 * certain data structures, useful for the computation. It tests also the configuration files parsing.
 */
class MetroParser extends AnyFunSuite {

  val paperDataset: List[DatasetRecord] = TextParser.textFileToListOfRecords("paperDataset")
  val myDataset: List[DatasetRecord] = TextParser.textFileToListOfRecords("myDataset")
  val metroDataset: List[DatasetRecord] = TextParser.textFileToListOfRecords("metro")

  test("Test paper dataset metro-like") {
    assert(paperDataset.size == 8)
    assert(myDataset.size == 7)
    assert(metroDataset.size == 2000)
  }

  val sensitivePaper: Map[String, List[String]] = TextParser.getSensitiveValuesFromConfig("paperDataset")
  val sensitiveMy: Map[String, List[String]] = TextParser.getSensitiveValuesFromConfig("myDataset")
  val sensitiveMetro: Map[String, List[String]] = TextParser.getSensitiveValuesFromConfig("metro")
  test("Test sensitive values metro-like") {
    assert(sensitiveMetro("0").size == 1)
    assert(sensitivePaper("0").size == sensitiveMy("0").size
      && sensitivePaper("0").size == 3)
  }
}

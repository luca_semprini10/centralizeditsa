package model.windowtrie

import model.Doublet

import scala.collection.mutable
import scala.collection.mutable.ListBuffer


object Trie {
  def apply() : Trie = new TrieNode()
}

/**
 * This trait models a Prefix Tree, useful to represent a Window, inside the ITSA algorithm.
 */
sealed trait Trie {
  var rootNode: Int = 0

  /**
   * Method to clear the whole Prefix Tree.
   */
  def empty(): Unit

  /**
   * Method to append a Window to the Prefix Tree.
   * @param key the Window to append in form of Map.
   */
  def append(key : Map[String, (ListBuffer[Doublet], Map[String, String])]): Unit

  /**
   * Method used to count all the instances of a given subsequence of Doublets inside the Prefix Tree.
   * @param subsequence the subsequence to count.
   * @return the number of the occurrences of the subsequence.
   */
  def countSubsequence(subsequence: List[Doublet]): Int

  /**
   * Method to count the all the instances of a given subsequence of Doublets having
   * a sensitive attribute value inside the Prefix Tree.
   * @param subsequence the subsequence to count.
   * @param g the number of occurrences of the subsequence inside the Prefix Tree: the support of the subsequence.
   * @param knownSensitiveValues the list of values of attributes that are known to be sensitive.
   * @return a map that pairs each sensitive value found inside the subsequence to its confidence
   */
  def countSubsequenceWithAttribute(subsequence: List[Doublet], g: Int,
                                    knownSensitiveValues: List[String]): Map[String, Double]
}

/**
 * This class represents a Node of the Prefix Tree.
 * @param doublet the doublet that the Node represents, as an Option value.
 * @param attributes the Map of attributes, paired with their values, of the given doublet.
 * @param ids the set of trajectory identifiers where the instances of the doublet appears.
 * @param isLeaf a boolean value telling wheater the doublet is a leaf or not.
 */
class TrieNode(val doublet : Option[Doublet] = None,
               var attributes: mutable.Map[String, mutable.Set[String]] = mutable.Map.empty,
               var ids: mutable.Set[String] = mutable.Set.empty,
               var isLeaf: Boolean = false) extends Trie {
  var children: mutable.Map[Doublet, TrieNode] = mutable.Map[Doublet, TrieNode]()

  private def updateIdsAndAttributes(node: TrieNode, doublet: Doublet, attributes: Map[String, String]): Unit = {
    node.ids += doublet.trajectoryId
    node.attributes.put(doublet.trajectoryId, mutable.Set.empty ++ attributes.values)
  }

  override def append(windowRecords: Map[String, (ListBuffer[Doublet], Map[String, String])]): Unit = {

    @scala.annotation.tailrec
    def appendHelper(trajectoryId: String, node: TrieNode, currentIndex: Int, newIndex: Int): Unit = {
      if(currentIndex >= windowRecords(trajectoryId)._1.length) {
        node.isLeaf = true
        this.updateIdsAndAttributes(node,
          windowRecords(trajectoryId)._1(currentIndex - 1),
          windowRecords(trajectoryId)._2)
        if(newIndex + 1 <= windowRecords(trajectoryId)._1.length)
          appendHelper(trajectoryId, this, newIndex + 1, newIndex + 1)
      } else {
        val doublet = windowRecords(trajectoryId)._1(currentIndex)
        this.updateIdsAndAttributes(node, doublet, windowRecords(trajectoryId)._2)
        val newNode = node.children.getOrElseUpdate(doublet, new TrieNode(Some(doublet)))
        appendHelper(trajectoryId, newNode, currentIndex + 1, newIndex)
      }
    }
    windowRecords.keys.foreach(k => {
      if(windowRecords(k)._1.nonEmpty) {
        appendHelper(k, this, 0, 0)
      }
    })
    this.rootNode = this.ids.size
  }

  private def idsPerPosition(subsequence: List[Doublet]): ListBuffer[ListBuffer[String]] = {
    subsequence.map(_ => new ListBuffer[String]).to[ListBuffer]
  }

  override def countSubsequence(subsequence: List[Doublet]): Int = {
    val idsPerPosition = this.idsPerPosition(subsequence)
    val doubletsFound: ListBuffer[Doublet] = ListBuffer.empty
    def countHelper(node: TrieNode, currentIndex: Int): Unit = {
      if (currentIndex < subsequence.size) {
        if (node.doublet.nonEmpty) {
          val nodeFound = node.doublet.get.equals(subsequence(currentIndex))
          if (nodeFound) {
            idsPerPosition(currentIndex) ++= node.ids
            if (!doubletsFound.contains(node.doublet.get)) {
              doubletsFound += node.doublet.get
            }
          }
          if (node.children.nonEmpty) {
            node.children.keys.foreach(k => {
              countHelper(node.children(k), if (nodeFound) currentIndex + 1 else currentIndex)
            })
          }
        }
      }
    }
    this.children.keys.foreach(k => {
      countHelper(this.children(k), 0)
    })

    if (doubletsFound.toList.equals(subsequence)) {
      idsPerPosition.map(_.distinct).minBy(_.size).size
    } else 0
  }

  override def countSubsequenceWithAttribute(subsequence: List[Doublet], g: Int,
                                             knownSensitiveValues: List[String]): Map[String, Double] = {
    val idsPerPosition = this.idsPerPosition(subsequence)
    val mapOfIds: Map[String, ListBuffer[ListBuffer[String]]] = knownSensitiveValues
      .map(value => value -> idsPerPosition).toMap
    val doubletsFound: Map[String, ListBuffer[Doublet]] = knownSensitiveValues
      .map(value => value -> new ListBuffer[Doublet]).toMap
    def countAttributeHelper(node: TrieNode, currentIndex: Int): Unit = {
      if (currentIndex < subsequence.size) {
        if (node.doublet.nonEmpty) {
          val nodeFound = node.doublet.get.equals(subsequence(currentIndex))
          if (nodeFound) {
            node.attributes.keys.foreach(k => {
              node.attributes(k).foreach(attribute => {
                if (knownSensitiveValues.contains(attribute)) {
                  mapOfIds(attribute)(currentIndex) += k
                  if (!doubletsFound(attribute).contains(node.doublet.get)) {
                    doubletsFound(attribute) += node.doublet.get
                  }
                }
              })
            })
          }
          if (node.children.nonEmpty) {
            node.children.keys.foreach(k => {
              countAttributeHelper(node.children(k), if (nodeFound) currentIndex + 1 else currentIndex)
            })
          }
        }
      }
    }
    this.children.keys.foreach(k => {
      countAttributeHelper(this.children(k), 0)
    })
    doubletsFound.map(attribute => {
      attribute._1 -> (
        if (doubletsFound(attribute._1).toList.equals(subsequence)) {
          val confidence = mapOfIds(attribute._1).map(_.distinct).minBy(_.size).size.toDouble / g.toDouble
          confidence
          //BigDecimal(confidence).setScale(1, BigDecimal.RoundingMode.HALF_UP).toDouble
        } else 0.0
        )
    })
  }

  override def empty(): Unit = {
    this.rootNode = 0
    this.ids.clear()
    this.attributes.clear()
    this.children.clear()
  }

  override def toString: String = s"\n$doublet (${ids.size} instances that are: $ids) " +
    s"with attributes $attributes, and children:\n\t$children"
}

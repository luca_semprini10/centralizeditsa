package model

/**
 * Class modeling an ideal record of a trajectory dataset: the dataset would have records containing
 * a person ID, a trajectory (a list of doublets having the same trajectory ID, equal to the person ID), and
 * a set of sensitive attributes, associated to their respective values.
 * @param personId the id of the person whose trajectory is represented in the dataset record.
 * @param trajectory the trajectory of the person (a list of doublets having the same trajectory ID, equal to the person ID).
 * @param senAtt a Map, where sensitive attributes are associated with their values.
 */
class DatasetRecord(val personId: Int, val trajectory: List[Doublet], val senAtt: Map[String, String])

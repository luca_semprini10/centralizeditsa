package model

import java.util.{Calendar, Date}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
 * Class modeling a sliding window over a dataset of trajectories. This window is based on time.
 * @param windowRecords the trajectories contained into the Window.
 * @param size the window temporal size, in form of minutes.
 * @param stepSize the window step size: how many minutes are involved into one window slide.
 * @param sampling the time sampling unit, in form of minutes. Optional value.
 */
class WindowRecords(var windowRecords: mutable.Map[String, (ListBuffer[Doublet], Map[String, String])],
                    val size: Int, val stepSize: Int, val sampling: Option[Int]) {

  var lastPositions: mutable.Map[String, Int] = mutable.Map()
  var doubletsWithoutNew: Map[Doublet, Set[String]] = Map()
  var newDoubletsUnique: mutable.Map[Doublet, Set[String]] = mutable.Map()
  var mapDoublets: mutable.Map[Doublet, Set[String]] = mutable.Map()
  var doneSliding: Boolean = false

  private var maxTime: Long = 0
  private var minTime: Long = 0
  private var lastTime: Long = 0
  private val c = Calendar.getInstance
  private val c2 = Calendar.getInstance

  private def setTimers(timer: Calendar, currentTime: Long, minutes: Int): Long = {
    timer.setTime(new Date(currentTime))
    timer.add(Calendar.MINUTE, minutes * sampling.getOrElse(WindowRecords.DEFAULT_SAMPLING))
    timer.getTimeInMillis
  }

  private def oldDoubletToRemove(d: Doublet): Boolean =
    new Date(d.timestamp).after(c.getTime) || new Date(d.timestamp).equals(c.getTime)

  private def updateMaps(doublet: Doublet, setOfIds: Set[String]): Unit = {
    val setToAdd = newDoubletsUnique.getOrElse(doublet, Set.empty)
    newDoubletsUnique.put(doublet, setToAdd ++ setOfIds)
    mapDoublets.put(doublet, setToAdd ++ setOfIds)
  }

  /**
   * Method to initialize the window the first time with a given dataset of trajectories.
   * @param dataset the whole dataset of trajectories.
   * @param firstTime the first time instant, which will be the lower temporal bound of the window.
   * @param lastTime the very last time instant of the dataset, which will be useful to stop the window sliding.
   */
  def initializeWindow(dataset: Dataset, firstTime: Long, lastTime: Long): Unit = {

    minTime = setTimers(c, firstTime, size)
    this.lastTime = lastTime
    minTime = firstTime

    val initialPosition = dataset.dataset.map(key => key._1 -> 0)
    windowRecords = mutable.Map() ++ dataset.dataset.map(key => key._1 -> (ListBuffer[Doublet](), key._2._2))
    dataset.dataset.keys.foreach(id => {
      @scala.annotation.tailrec
      def setLastPosition(trajectory: List[Doublet], currentPosition: Int, time: Date): Unit = {
        lastPositions(id) = currentPosition
        if (trajectory.size > currentPosition) {
          if (new Date(trajectory(currentPosition).timestamp).before(time)) {
            if(maxTime < trajectory(currentPosition).timestamp) maxTime = trajectory(currentPosition).timestamp
            windowRecords(id)._1 += trajectory(currentPosition)
            this.updateMaps(trajectory(currentPosition), Set(id))
            setLastPosition(trajectory, currentPosition + 1, time)
          }
        }
      }
      setLastPosition(dataset.dataset(id)._1, initialPosition(id), c.getTime)
      windowRecords(id) = (windowRecords(id)._1, dataset.dataset(id)._2)
      doubletsWithoutNew = mapDoublets.toMap
    })
  }

  /**
   * Method to slide the window over the dataset of step size minutes.
   * @param dataset the whole dataset of trajectories.
   */
  def slideWindow (dataset: Dataset): Unit = {

    minTime = setTimers(c, minTime, stepSize)
    maxTime = setTimers(c2, maxTime, stepSize)

    mapDoublets.keys
      .filterNot(d => oldDoubletToRemove(d))
      .foreach(k => mapDoublets -= k)
    doubletsWithoutNew = mapDoublets.toMap
    newDoubletsUnique = mutable.Map()
    doneSliding = c2.getTimeInMillis >= lastTime

    val newDoubletsMap: mutable.Map[String, (ListBuffer[Doublet], Map[String, String])] = mutable.Map()
    dataset.dataset.keys.foreach( id => {
      if (!newDoubletsMap.contains(id)) newDoubletsMap(id) = (ListBuffer.empty, dataset.dataset(id)._2)
      windowRecords(id) = (windowRecords(id)._1
        .filter(p => oldDoubletToRemove(p)), windowRecords(id)._2)
      @scala.annotation.tailrec
      def slidePositions (trajectory: List[Doublet], currentLast: Int, timeLast: Date): Unit = {
        lastPositions(id) = currentLast
        if (lastPositions(id) < trajectory.size) {
          if (new Date(trajectory(currentLast).timestamp).before(timeLast) ||
            new Date(trajectory(currentLast).timestamp).equals(timeLast)) {
            if (!newDoubletsMap(id)._1.exists(d => trajectory(currentLast).strictEquals(d))) {
              this.updateMaps(trajectory(currentLast), Set(id))
              windowRecords(id)._1 += trajectory(currentLast)
              newDoubletsMap(id)._1 += trajectory(currentLast)
            }
            slidePositions(trajectory, currentLast + 1, timeLast)
          }
        }
      }
      if (stepSize < size) {
        slidePositions(dataset.dataset(id)._1, lastPositions(id), c2.getTime)
      }
    })
  }

  override def toString: String = s"Window size $size & step size $stepSize:" +
    s" there are ${mapDoublets.values.map(_.size).sum} records" +
    s"\n${windowRecords.values.filter(_._1.nonEmpty)}"
}

object WindowRecords {
  final val DEFAULT_SAMPLING: Int = 1
}

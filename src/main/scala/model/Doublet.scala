package model

import java.util
import java.util.Date

import scala.collection.mutable.ListBuffer

/**
 * Class modeling a Doublet: this is a spatio-temporal unit representing a point in the space at a given time.
 * A list of doublets with the same trajectory ID, sorted by time, represents the trajectory of a given person.
 * @param trajectoryId the ID of the trajectory to which the Doublet belongs.
 * @param latitude the latitude value of the space dimension of the Doublet.
 * @param longitude the longitude value of the space dimension of the Doublet.
 * @param timestamp the time value of the temporal dimension of the Doublet.
 */
class Doublet(val trajectoryId: String, val latitude: Double, val longitude: Double, val timestamp: Long) extends Comparable[Doublet]{

  override def toString: String = s"id=$trajectoryId [$latitude, $longitude];${new Date(timestamp)}"

  override def compareTo(t: Doublet): Int =
    this.latitude.compareTo(t.latitude) +
      this.longitude.compareTo(t.longitude) +
      this.timestamp.compareTo(t.timestamp)

  override def equals(t: Any): Boolean =
    t match {
      case doublet: Doublet =>
        this.latitude.equals(doublet.latitude) &&
          this.longitude.equals(doublet.longitude) &&
          this.timestamp.equals(doublet.timestamp)
      case _ => false
    }

  override def hashCode(): Int = {
    val state = Seq(latitude, longitude, timestamp.toDouble)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }

  /**
   * Method to give a lexicographical representation of a Doublet, so that it can be
   * sorted along with other doublets.
   * @return a String representing the time and space dimensions of the Doublet.
   */
  def toSort: String = s"[$latitude, $longitude];$timestamp"

  /**
   * Method to extend the equals method for a Doublet: this considers also the trajectory ID.
   * @param d the object to verify.
   * @return true if the object is strictly equals to the Doublet, false otherwise.
   */
  def strictEquals(d: Any): Boolean = d match {
    case doublet: Doublet =>
      this.trajectoryId.equals(doublet.trajectoryId) &&
        this.longitude.equals(doublet.longitude) &&
        this.latitude.equals(doublet.latitude) &&
        this.timestamp.equals(doublet.timestamp)
    case _ => false

  }
}
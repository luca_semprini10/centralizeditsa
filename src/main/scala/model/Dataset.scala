package model

/**
 * Class modeling a dataset: a Map, where to every trajectory id (key) is associated a Pair
 * of list of Doublets (trajectory) and its attributes, associated with their values.
 * @param dataset the dataset structure.
 */
case class Dataset(dataset: Map[String, (List[Doublet], Map[String, String])])

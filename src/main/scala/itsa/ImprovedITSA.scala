package itsa

import model.windowtrie.{Trie, TrieNode}
import model.{Dataset, Doublet, WindowRecords}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.{Map => MutMap}

object ImprovedITSA {

  private var debug: Boolean = false
  var suppressedDoublets: mutable.Set[Doublet] = mutable.Set.empty
  var totalSuppressed: Int = 0
  var distortionRatio: Double = 0

  /**
   * Method to fill the List of anonymous Windows.
   * @param window the anonymous Window to be published, in form of List of Doublets.
   */
  private def publishWindow(window: Map[Doublet, Set[String]],
                            windowsToPublish: ListBuffer[Map[Doublet, Set[String]]]): Unit = {
    windowsToPublish += window
    println(s"Published Window number ${windowsToPublish.size}")
  }

  /**
   * Method to self-join a list of sequences, to create a list of sequences with a greater cardinality.
   * @param window the current Window.
   * @param uniqueDoublets the unique doublets in the Window that are not present in the set of new doublets added with the slide.
   * @param candidates the list of sequences to be self-joined.
   * @return a new list of sequences, which will have sequences with higher size.
   */
  private def selfJoinCandidates(window: WindowRecords, uniqueDoublets: Map[Doublet, Set[String]],
                         candidates: MutMap[ListBuffer[Doublet], mutable.Set[String]]):
  MutMap[ListBuffer[Doublet], mutable.Set[String]] = {
    val sequencesJoined: MutMap[ListBuffer[Doublet], mutable.Set[String]] = MutMap.empty
    candidates.keys.foreach(sequence => {
      uniqueDoublets.keys.foreach(un => {
        val sequenceWithoutLast = sequence.slice(0, sequence.size - 1)
        if (sequenceWithoutLast.last.timestamp < un.timestamp) {
          val ids = candidates(sequence).intersect(uniqueDoublets(un))
          if(ids.nonEmpty) {
            val sequenceToAdd = (sequenceWithoutLast += un) += sequence.last
            sequencesJoined.put(sequenceToAdd, ids)
          }
        }
      })
    })
    sequencesJoined
  }

  /**
   * Method to get the subsets of size N from a set of Doublets.
   * @param unique the list of unique doublets to iterate from.
   * @param previousSubsets the list of the previous subsets found, which sizes must be incremented.
   * @return a list of subsets of Doublets.
   */
  private def subsetsOfN(unique: Map[Doublet, Set[String]], previousSubsets: MutMap[ListBuffer[Doublet], Set[String]]):
  MutMap[ListBuffer[Doublet], Set[String]]  = {
    var listToReturn: MutMap[ListBuffer[Doublet], Set[String]] = MutMap.empty
    if (previousSubsets.isEmpty) listToReturn ++= unique.keys.map(ListBuffer(_)).map(d => d -> unique(d.head)).toMap
    else {
      previousSubsets.keys.foreach(sequence => {
        unique.keys.filter(d => d.timestamp > sequence.last.timestamp)
          .foreach(un => {
            val ids = unique(un).intersect(previousSubsets(sequence))
            if(ids.nonEmpty) {
              val sequenceToAdd = sequence :+ un
              listToReturn.put(sequenceToAdd, ids)
            }
          })
      })
    }
    listToReturn
  }
  /**
   * Method to create the first list of candidates, by obtaining a cartesian product of the unique doublets
   * in the Window that are not present in the set of new doublets added with the slide and this set of new doublets.
   * @param unique the unique doublets in the Window that are not present in the set of new doublets added with the slide.
   * @param candidates the set of new doublets added with the slide.
   * @return a list of 2-size sequences of doublets.
   */
  private def joinDoubletSequence2(unique: Map[Doublet, Set[String]],
                                   candidates: Map[Doublet, Set[String]]):
  MutMap[ListBuffer[Doublet], mutable.Set[String]] = {
    val listToReturn: MutMap[ListBuffer[Doublet], mutable.Set[String]] = MutMap.empty
    unique.keys.foreach(l => {
      candidates.keys.filter(d => d.timestamp > l.timestamp)
        .foreach(e => {
          val ids = unique(l).intersect(candidates(e))
          if(ids.nonEmpty) {
            val newSequence = ListBuffer(l, e)
            listToReturn.put(newSequence, mutable.Set.empty ++ ids)
          }
      })
    })
    listToReturn
  }

  /**
   * Method to check if a given subset consists in a violation of privacy.
   * @param windowTrie the prefix Trie representing the current window
   * @param knownSensitiveValues a Map where, for every attribute, is listed a set of values that are known to be sensitive.
   * @param subset the sequence of doublets to be verified.
   * @param thresholdC the threshold C of the LKC-privacy principle.
   * @param thresholdK the threshold K of the LKC-privacy principle.
   * @return a boolean value that says weather the sequence is a violation or not.
   */
  private def checkViolation(windowTrie: Trie, knownSensitiveValues: Map[String, List[String]],
                     subset: List[Doublet], thresholdC: Double, thresholdK: Int): Boolean = {
    val ge = windowTrie.countSubsequence(subset)
    val confidencesForEachAttribute = windowTrie.countSubsequenceWithAttribute(subset, ge,
      knownSensitiveValues.values.flatten.toList)
    val violationForConfidence = confidencesForEachAttribute.values.toList.exists(_ > thresholdC)
    ge < thresholdK || violationForConfidence
  }

  /**
   * Method to select a list of winner doublets to be suppressed from a given list of doublets.
   * @param windowTrie the prefix Trie representing the current Window.
   * @param knownSensitiveValues a Map where, for every attribute, is listed a set of values that are known to be sensitive.
   * @param violations the list of current violations.
   * @param thresholdK the threshold K of the LKC-privacy principle.
   * @param thresholdC the threshold C of the LKC-privacy principle.
   * @return the list of winner doublets to be suppressed.
   */
  private def findWinners(windowTrie: Trie, knownSensitiveValues: Map[String, List[String]],
                  violations: ListBuffer[ListBuffer[Doublet]],
                  thresholdK: Int, thresholdC: Double): Set[Doublet] = {

    def computeGain(violations1: ListBuffer[ListBuffer[Doublet]],
                    flatViolations1: ListBuffer[Doublet]): Map[Doublet, Double] =
      flatViolations1.map(el => el -> violations1.count(_.contains(el)).toDouble).toMap

    def computeInfoLoss(flatViolations1: ListBuffer[Doublet], trie: Trie): Map[Doublet, Double] =
      flatViolations1.map(el => el -> trie.countSubsequence(List(el)).toDouble).toMap

    def computeScoreTable(gainMap1: Map[Doublet, Double],
                          supportMap1: Map[Doublet, Double]): Map[Doublet, Double] =
      supportMap1 ++ gainMap1.map(entry => entry._1 -> (entry._2 / supportMap1.getOrElse(entry._1, 1.0)))

    val flatViolations = violations.flatten.distinct
    var oldViolations: List[List[Doublet]] = violations.map(_.toList).toList
    val gainMap = computeGain(violations, flatViolations)
    val supportMap = computeInfoLoss(flatViolations, windowTrie)
    var scoreTable = computeScoreTable(gainMap, supportMap)
    val winners: mutable.Set[Doublet] = mutable.Set()

    if (debug) {
      println("----------------")
      println(s"VIOLATIONS ARE:")
      oldViolations.foreach(println(_))
    }

    do {
      val w: Doublet = scoreTable
        .maxBy(entry => entry)(
          Ordering.by { it => (it._2, it._1.toSort) }
        )._1
      if (debug) println(s"Winner $w with score ${scoreTable(w)}\nSupport: ${supportMap(w)}\nGain: ${gainMap(w)}")
      winners += w
      violations.filter(v => v.contains(w)).foreach(l => {
        l -= w
        if (l.isEmpty) violations -= l
      })
      scoreTable = scoreTable.-(w)

      oldViolations.filter(_.contains(w)).foreach(e => {
        val violationRecomputed = e.filterNot(_.equals(w))
        if(violationRecomputed.nonEmpty) {
          if (!checkViolation(windowTrie, knownSensitiveValues, violationRecomputed, thresholdC, thresholdK))
            violations -= violationRecomputed.to[ListBuffer]
        }
      })
      oldViolations = violations.map(_.toList).toList
      val flatViolations = violations.flatten.distinct
      val newGain = computeGain(violations, flatViolations)
      val newLoss = computeInfoLoss(flatViolations, windowTrie)
      scoreTable = computeScoreTable(newGain, newLoss)
    } while (scoreTable.nonEmpty)

    winners.toSet
  }

  /**
   * Method that suppresses all the instances of a certain doublet from a list of doublets
   * @param doublet the doublet to be suppressed.
   * @param window the Window to suppress the doublet from.
   * @return the list of doublets without all instances of the given doublet.
   */
  private def suppress(doublet: Doublet, window: WindowRecords): Unit = {
    this.suppressedDoublets += doublet
    totalSuppressed += window.mapDoublets(doublet).size
    window.mapDoublets -= doublet
    window.newDoubletsUnique -= doublet
    window.windowRecords = window.windowRecords.map(d => d._1 -> (d._2._1.filterNot(_.equals(doublet)), d._2._2))
  }

  /**
   * Method that first finds winner doublets and then suppresses them from the Window.
   * @param windowTrie the prefix Trie representing the Window.
   * @param knownSensitiveValues a Map where, for every attribute, is listed a set of values that are known to be sensitive.
   * @param violations the list of current violations.
   * @param window the not-anonymous Window.
   * @param thresholdK the threshold K of the LKC-privacy principle.
   * @param thresholdC the threshold C of the LKC-privacy principle.
   */
  private def suppressWinners(windowTrie: Trie, knownSensitiveValues: Map[String, List[String]],
                      violations: ListBuffer[ListBuffer[Doublet]],
                      window: WindowRecords,
                      thresholdK: Int, thresholdC: Double): Unit = {
    val winners = findWinners(windowTrie, knownSensitiveValues, violations, thresholdK, thresholdC)
    winners.foreach(w => {
      suppress(w, window)
      if (debug) println(s"Suppressed $w from Window")
    })
  }

  /**
   * Method to apply the LKC-privacy principle to a list of doublets, anonymizing it.
   * @param knownSensitiveValues a Map where, for every attribute, is listed a set of values that are known to be sensitive.
   * @param window the new doublets added to the Window as it has slided.
   * @param minStepSizeL the minimum value between the threshold L of the LKC-privacy principle and the step_size.
   * @param thresholdK the threshold K of the LKC-privacy principle.
   * @param thresholdC the threshold C of the LKC-privacy principle.
   * @return the anonymous list of doublets
   */
  private def anonymizeNew(windowTrie: Trie, knownSensitiveValues: Map[String, List[String]],
                   window: WindowRecords, minStepSizeL: Int, thresholdC: Double, thresholdK: Int): Unit = {
    var violations: ListBuffer[ListBuffer[Doublet]] = ListBuffer.empty
    var subsetsSaved: MutMap[ListBuffer[Doublet], Set[String]] = MutMap.empty
    val uniqueNew = window.newDoubletsUnique
    for (i <- 1 to minStepSizeL) {
      val subsets = subsetsOfN(uniqueNew.toMap, subsetsSaved)
      if (debug) println(s"Subsets of size $i generated")
      subsetsSaved = subsets
      subsets.keys.foreach(e => {
        if (checkViolation(windowTrie, knownSensitiveValues, e.toList, thresholdC, thresholdK)) {
          violations += e
          subsetsSaved -= e
          e.foreach(uniqueNew -= _)
        }
      })
      if (debug) println(s"Violations generated for subsets of size $i")
    }
    if (violations.nonEmpty) {
      suppressWinners(windowTrie, knownSensitiveValues, violations, window, thresholdK, thresholdC)
    }
  }

  /**
   * Method to anonymize a dataset in a sliding Window fashion, using LKC-privacy principle and the ITSA algorithm.
   * @param window the not-anonymous Window.
   * @param mapDataset the dataset, represented by a map.
   * @param knownSensitiveValues a Map where, for every attribute, is listed a set of values that are known to be sensitive.
   * @param thresholdL the threshold L of the LKC-privacy principle.
   * @param thresholdK the threshold K of the LKC-privacy principle.
   * @param thresholdC the threshold C of the LKC-privacy principle.
   * @return a list of anonymous Windows, covering the whole given dataset.
   */
  def doITSA(window: WindowRecords,
             mapDataset: Dataset,
             knownSensitiveValues: Map[String, List[String]],
             thresholdL: Int, thresholdK: Int,
             thresholdC: Double,
             totalDoublets: BigInt,
             debug: Boolean): List[Map[Doublet, Set[String]]] = {
    this.debug = debug
    this.suppressedDoublets = mutable.Set.empty
    this.totalSuppressed = 0
    val windowsToPublish: ListBuffer[Map[Doublet, Set[String]]] = ListBuffer.empty
    window.initializeWindow(mapDataset,
      mapDataset.dataset.values.flatMap(_._1).minBy(d => d.timestamp).timestamp,
      mapDataset.dataset.values.flatMap(_._1).maxBy(d => d.timestamp).timestamp)
    if (debug) println("Window initialized")
    val windowTrie = new TrieNode()
    windowTrie.append(window.windowRecords.toMap)
    if (debug) println("Prefix tree created")
    anonymizeNew(windowTrie, knownSensitiveValues, window, Math.min(window.size, thresholdL), thresholdC, thresholdK)
    publishWindow(window.mapDoublets.toMap, windowsToPublish)
    while (!window.doneSliding && thresholdL <= window.size) {
      window.slideWindow(mapDataset)
      if (debug) println(s"Window slided: current window is window number ${windowsToPublish.size + 1}" +
        s" with ${window.mapDoublets.values.map(_.size).sum} doublets")
      if (window.windowRecords.nonEmpty && window.newDoubletsUnique.keys.nonEmpty) {
        if (debug) {
          // ********** memory info ************
          val mb = 1024*1024
          val runtime = Runtime.getRuntime
          println("** Used Memory:  " + (runtime.totalMemory - runtime.freeMemory) / mb)
          println("** Free Memory:  " + runtime.freeMemory / mb)
          println("** Total Memory: " + runtime.totalMemory / mb)
          println("** Max Memory:   " + runtime.maxMemory / mb)
          // ***********************************
        }
        windowTrie.empty()
        if (debug) println("Prefix tree cleared")
        windowTrie.append(window.windowRecords.toMap)
        if (debug) println("Prefix tree created")
        //Phase 1 begins
        anonymizeNew(windowTrie, knownSensitiveValues, window, Math.min(window.stepSize, thresholdL), thresholdC, thresholdK)
        val candidates1 = window.doubletsWithoutNew
        var candidatesI = joinDoubletSequence2(candidates1, window.newDoubletsUnique.toMap)
        var violations: ListBuffer[ListBuffer[Doublet]] = ListBuffer.empty
        var i = 2
        //Phase 2 begins
        if(thresholdL > 1) {
          do {
            if (debug) println(s"Candidates of size $i generated")
            candidatesI.keys.foreach(q => {
              if (violations.exists(v => q.containsSlice(v))) {
                candidatesI -= q
                violations += q
              } else {
                if (checkViolation(windowTrie, knownSensitiveValues, q.toList, thresholdC, thresholdK)) {
                  candidatesI -= q
                  violations += q
                }
              }
            })
            i += 1
            if (i <= thresholdL) {
              candidatesI = selfJoinCandidates(window, candidates1, candidatesI)
            }
          } while (i <= thresholdL && candidatesI.nonEmpty)
        }

        //Phase 3 begins
        if (violations.isEmpty) publishWindow(window.mapDoublets.toMap, windowsToPublish)
        else {
          if (debug) println(s"Violations found in Window ${windowsToPublish.size + 1}!")
          suppressWinners(windowTrie, knownSensitiveValues, violations, window, thresholdK, thresholdC)
          publishWindow(window.mapDoublets.toMap, windowsToPublish)
        }
      }
    }
    if (debug) println("Anonymization finished, calculating distortion ratio")
    distortionRatio = BigDecimal((100 * totalSuppressed).toDouble / totalDoublets.toDouble)
      .setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
    windowsToPublish.toList
  }
}


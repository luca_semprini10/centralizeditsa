package conf

import org.rogach.scallop.{ScallopConf, ScallopOption}

/**
 * Class to be used to parse CLI commands, the values declared inside specify name and type of the arguments to parse.
 *
 * @param arguments the programs arguments as an array of strings.
 */
class MainConf(arguments: Array[String]) extends ScallopConf(arguments) {
  val threshold_l: ScallopOption[Int] = opt[Int](required = true)
  val threshold_k: ScallopOption[Int] = opt[Int](required = true)
  val threshold_c: ScallopOption[Double] = opt[Double](required = true)
  val window_size_n: ScallopOption[Int] = opt[Int](required = true)
  val step_size: ScallopOption[Int] = opt[Int](required = true)
  val dataset: ScallopOption[String] = opt[String](required = true)
  val rounding: ScallopOption[Int] = opt[Int]()
  val rounding_olden: ScallopOption[Int] = opt[Int]()
  val max_time_olden: ScallopOption[Int] = opt[Int]()
  val debug: ScallopOption[Boolean] = opt[Boolean]()
  val num_rows: ScallopOption[Int] = opt[Int]()
  val sampling: ScallopOption[Int] = opt[Int]()
  val exec_memory: ScallopOption[Int] = opt[Int]()
  val executors: ScallopOption[Int] = opt[Int]()
  val driver_memory: ScallopOption[Int] = opt[Int]()
  val kryo_max: ScallopOption[Int] = opt[Int]()
  verify()
}
package utils

/**
 * Utility class with useful Strings, mostly input dataset names.
 */
object StringUtils {

  final val PROJECT_NAME = "centralizeditsa"
  final val METRO_DATASET = "MetroDataSet"
  final val PAPER_DATASET = "paperDataset"
  final val SIMPLE_METRO = "metro"
  final val CARIPLO_TABLE = "cariploenr6"
  final val CARIPLO_TABLE_SAMPLE = s"${CARIPLO_TABLE}_sample_1min"
  final val OLDENBURG_TABLE = "final_oldenburg_dataset"
  final val IDS_SEPARATOR = "-"
  final val IS_METRO_LIKE: String => Boolean =
    (dataset: String) => dataset == METRO_DATASET ||
      dataset == PAPER_DATASET || dataset == SIMPLE_METRO
  final val IS_CARIPLO: String => Boolean =
    (dataset: String) => dataset == CARIPLO_TABLE || dataset == CARIPLO_TABLE_SAMPLE
  final val IS_OLDENBURG: String => Boolean = (dataset:String) => dataset == OLDENBURG_TABLE
}

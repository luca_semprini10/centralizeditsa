import conf.MainConf
import itsa.ImprovedITSA
import model.{Dataset, Doublet, WindowRecords}
import org.apache.spark.sql.{DataFrame, SparkSession}
import parser.{CsvOutputWriter, TextParser}
import sparksql.HiveTableParser
import utils.StringUtils
import scala.collection.mutable

object Main {

  private final val emptyDataset: (Dataset, BigInt) = (Dataset(Map.empty), 0)

  def main(args: Array[String]): Unit = {
    if(args.length < 6) {
        illegalArguments("Not Enough Arguments!!", Option(s"Found: $args"))
    } else {
      val conf = new MainConf(args)
      val datasetName = conf.dataset()
      val knownSensitiveValues: Map[String, List[String]] =
        if(StringUtils.IS_METRO_LIKE(datasetName)) TextParser.getSensitiveValuesFromConfig(datasetName)
        else Map.empty
      val datasetAndDoublets: (Dataset, BigInt) =
        if(StringUtils.IS_METRO_LIKE(datasetName))
          TextParser.textFileToMapOfRecords(datasetName, conf.num_rows.toOption)
        else if(StringUtils.IS_CARIPLO(datasetName) || StringUtils.IS_OLDENBURG(datasetName)) {
          val sparkSession: SparkSession = HiveTableParser.generateSparkSession(conf.exec_memory.toOption,
            conf.executors.toOption, conf.driver_memory.toOption, conf.kryo_max.toOption)
          val dataFrame: DataFrame = HiveTableParser.getDataFrame(conf.num_rows.toOption,
            sparkSession, datasetName, conf.max_time_olden.toOption)
          val parsedDataset = HiveTableParser.modelDataFrame(dataFrame, datasetName,
            conf.rounding.toOption, conf.rounding_olden.toOption, conf.sampling.toOption)
          val totalD: BigInt =
            if (parsedDataset._2 == 0) parsedDataset._1.dataset.map(_._2._1.size).sum else parsedDataset._2
          (parsedDataset._1, totalD)
        } else emptyDataset
      if(datasetAndDoublets == emptyDataset)
        illegalArguments("WRONG DATASET SPECIFIED!!", Option(s"Found: $datasetName"))

      if(conf.debug()) {
        println("Total doublets: " + datasetAndDoublets._2)
        println("Total trajectories: " + datasetAndDoublets._1.dataset.keys.size)
      }
      val timeStart = System.nanoTime
      val windowsPublishedImproved: List[Map[Doublet, Set[String]]] = ImprovedITSA.doITSA(
        new WindowRecords(mutable.Map.empty, conf.window_size_n(), conf.step_size(), conf.sampling.toOption),
        datasetAndDoublets._1,
        knownSensitiveValues,
        conf.threshold_l(),
        conf.threshold_k(),
        conf.threshold_c(),
        datasetAndDoublets._2,
        conf.debug()
      )
      val duration = (System.nanoTime - timeStart) / 1e9d
      if(conf.debug()) {
        println("Total doublets: " + datasetAndDoublets._2)
        println("Total trajectories: " + datasetAndDoublets._1.dataset.keys.size)
        println(s"There are ${windowsPublishedImproved.size} windows")
        println(s"total doublets suppressed: ${ImprovedITSA.totalSuppressed}")
        println(s"Execution took $duration seconds")
        println(s"Distortion ratio: ${ImprovedITSA.distortionRatio}%")
        for((_,i) <- windowsPublishedImproved.view.zipWithIndex) {
          println(s"--------Window ${i + 1}--------")
          val totalDoubletsInWindow = windowsPublishedImproved(i).values.map(_.size).toList.sum
          println(totalDoubletsInWindow)
        }
      }

      val listOfRecords = CsvOutputWriter.parseOutputDataset(ImprovedITSA.suppressedDoublets.toSet,
        datasetAndDoublets._1, datasetName, conf.sampling.toOption)
      CsvOutputWriter.callWriteFunction(conf, datasetAndDoublets._1.dataset.keys.size,
        ImprovedITSA.distortionRatio, isDataset = true, listOfRecords._1)
      if(listOfRecords._2.nonEmpty) {
        CsvOutputWriter.callWriteFunction(conf, datasetAndDoublets._1.dataset.keys.size,
          ImprovedITSA.distortionRatio, isDataset = false, listOfRecords._2.get)
      }
    }
  }

  private def illegalArguments(error: String, otherString: Option[String]): Unit = {
    throw new IllegalArgumentException(s"$error \n" +
      "USAGE: ./CentralizedITSA-1.0.0-spark.jar " +
      "[THRESHOLD L] -> Integer Value \n" +
      "[THRESHOLD K] -> Integer Value \n" +
      "[THRESHOLD C] -> Double Value \n " +
      "[WINDOW SIZE N] -> Integer Value \n" +
      "[WINDOW STEP SIZE] -> Integer Value\n" +
      "[TABLE ON HIVE] -> String Value\n" +
      "[NUMBER OF DECIMAL PLACES IN LATITUDE & LONGITUDE] -> Integer Value\n" +
      "[DEBUG] -> Boolean value: true/false\n" +
      "<OPTIONAL> [NUMBER OF LIMITED UNIQUE TRAJECTORIES] -> Integer Value\n" +
      "<OPTIONAL> [WINDOW SAMPLING IN MINUTES] -> Integer Value\n" +
      "<OPTIONAL> [ROUNDING OLDENBURG] -> Integer Value\n" +
      "<OPTIONAL> [MAX TIME IN OLDENBURG] -> Integer Value\n" +
      "<OPTIONAL> [SPARK EXECUTORS MEMORY] -> Integer Value\n" +
      "<OPTIONAL> [SPARK NUMBER OF EXECUTORS] -> Integer Value\n" +
      "<OPTIONAL> [SPARK DRIVER MEMORY] -> Integer Value\n" +
      "<OPTIONAL> [KRYO BUFFER SIZE] -> Integer Value\n" +
      s"${otherString.getOrElse("")}")
  }

}

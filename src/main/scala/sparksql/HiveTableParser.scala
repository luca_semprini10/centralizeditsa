package sparksql

import java.util.{Calendar, Date}

import model.{Dataset, Doublet}
import org.apache.spark.sql.{DataFrame, SparkSession}
import utils.StringUtils

/**
 * Object that has the purpose to connect the program to Hive, generate a custom SparkSession
 * and parse DataFrames to centralized data structures, to be given to the ITSA algorithm as an input.
 */
object HiveTableParser {

  private final val DATABASE: String = "trajectory"
  private final val LATITUDE: String = "latitude"
  private final val LONGITUDE: String = "longitude"
  private final val TIMESTAMP: String = "timest"
  private final val TRAJECTORY_ID: String = "trajid"
  private final val USER_ID: String = "customid"
  private final val DOUBLETS: String = "doublets"
  private final val CHAR_TO_SPLIT: String = ";"
  private final val DATE_FORMAT = "yyyy-MM-dd_HH:mm"

  final val ARBITRARY_DATE = "2020-01-01_00:00"
  final val format = new java.text.SimpleDateFormat(DATE_FORMAT)

  private def roundedCoordinate(coordinate: Double, approximation: Int, furtherRounding: Option[Int]): Double = {
    Math.round(BigDecimal(coordinate)
      .setScale(approximation, BigDecimal.RoundingMode.HALF_UP)
      .toDouble / furtherRounding.getOrElse(1)) * furtherRounding.getOrElse(1)
  }

  private def roundedTimestamp(timestamp: Date, approximation: Int): Long =
    Math.round(timestamp.getTime.toDouble / (approximation * 60 * 1000)) * (approximation * 60 * 1000)

  private def instantToTimestamp(instant: Int, approximation: Option[Int]): Long = {
    val c = Calendar.getInstance
    c.setTime(format.parse(ARBITRARY_DATE))
    c.add(Calendar.MINUTE, instant * approximation.getOrElse(1))
    c.getTime.getTime
  }

  /**
   * Method to generate a custom SparkSession.
   * @param executorMemory the memory to allocate to each executor. Optional value, default is 5GB.
   * @param executors the number of total executors. Optional value, default is 5.
   * @param driverMemory the Spark driver memory. Optional value, default is 5GB.
   * @param kryoMax the max Kryo buffer size. Optional value, default is 512MB.
   * @return the custom SparkSession.
   */
  def generateSparkSession(executorMemory: Option[Int],
                           executors: Option[Int],
                           driverMemory: Option[Int],
                           kryoMax: Option[Int]): SparkSession =
    SparkSession.builder
      .config("spark.executor.memory", s"${executorMemory.getOrElse(5)}g")
      .config("spark.executor.instances", s"${executors.getOrElse(5)}")
      .config("spark.driver.memory", s"${driverMemory.getOrElse(5)}g")
      .config("spark.kryoserializer.buffer.max", s"${kryoMax.getOrElse(512)}m")
      .config("spark.dynamicAllocation.enabled", "false")
      .enableHiveSupport()
      .getOrCreate()

  /**
   * Method able to query multiple Hive tables, in order to get a DataFrame of trajectories.
   * @param numRows the limited number of dataset rows to take. Optional value.
   * @param sparkSession the current SparkSession.
   * @param table the table name to query.
   * @param limitedTime the limited timestamp further which exclude doublets.
   * @return the DataFrame of trajectories.
   */
  def getDataFrame(numRows: Option[Int], sparkSession: SparkSession,
                   table: String, limitedTime: Option[Int]): DataFrame = {
    val timestamp: String = if (StringUtils.IS_CARIPLO(table)) TIMESTAMP else "timestamp"
    val ids: String = if (StringUtils.IS_CARIPLO(table)) s"$USER_ID," else ""
    val idsInS: String = if (StringUtils.IS_CARIPLO(table)) s"s.$USER_ID, " else ""
    val timestampFormatted =
      if (StringUtils.IS_CARIPLO(table))
        s"date_format(from_unixtime(cast($timestamp as bigint)), '$DATE_FORMAT')"
      else s"$timestamp"

    sparkSession.sqlContext.sql(s"select $ids $TRAJECTORY_ID, " +
      s"concat('{', concat_ws('$CHAR_TO_SPLIT', max(g)), '}') as $DOUBLETS " +
      s"from (select $idsInS s.$TRAJECTORY_ID, collect_list(s.c) over " +
      s"(partition by $idsInS s.$TRAJECTORY_ID order by s.$timestamp) g " +
      s"from (select $ids $TRAJECTORY_ID, $LATITUDE, $LONGITUDE, $timestamp, " +
      s"concat('(', concat_ws(',', $LATITUDE, $LONGITUDE, $timestampFormatted), ')') as c " +
      s"from $DATABASE.$table " +
        (if(limitedTime.nonEmpty && StringUtils.IS_OLDENBURG(table)) s"where $timestamp < ${limitedTime.get}"
        else "") +
      s")s )gs group by $ids $TRAJECTORY_ID " +
      (if(numRows.nonEmpty) s"limit ${numRows.get}" else ""))
  }

  /**
   * Method to collect the DataFrame and parse it into a centralized data structure.
   * @param dataFrame the DataFrame of trajectories.
   * @param tableName the name of the table that has been queried.
   * @param approximationCoordinate number of decimal ciphers to be considered from each coordinate. Optional value
   * @param roundingOldenburg number against which round the coordinates in case of Oldenburg dataset. Optional value.
   * @param approximationTimestamp timestamp sampling, in minutes.
   * @return a Map of trajectories, to be given to ITSA as an input structure.
   */
  def modelDataFrame(dataFrame: DataFrame,
                     tableName: String,
                     approximationCoordinate: Option[Int],
                     roundingOldenburg: Option[Int],
                     approximationTimestamp: Option[Int]): (Dataset, BigInt) = {
    var totalDoublets: BigInt = 0
    val mapToReturn = dataFrame
      .rdd
      .map(row => {
        (if (StringUtils.IS_CARIPLO(tableName)) row.getAs[String](USER_ID)
          + StringUtils.IDS_SEPARATOR else "") +
          row.getAs[Int](TRAJECTORY_ID).toString ->
        row.getAs[String](DOUBLETS)
      })
      .map(it => it._1 ->
        (it._2.split(CHAR_TO_SPLIT).map(doublet => {
          val doubletString = doublet
            .replaceAll("\\(", "")
            .replaceAll("\\)", "")
            .replaceAll("\\{", "")
            .replaceAll("}", "")
            .replaceAll(" ", "")
            .split(",")

          totalDoublets += 1

          new Doublet(trajectoryId = it._1,
            latitude =
              if(approximationCoordinate.nonEmpty)
                roundedCoordinate(doubletString(0).toDouble,
                  approximationCoordinate.get, roundingOldenburg)
              else doubletString(0).toDouble,
            longitude =
              if(approximationCoordinate.nonEmpty)
                roundedCoordinate(doubletString(1).toDouble,
                  approximationCoordinate.get, roundingOldenburg)
              else doubletString(1).toDouble,
            timestamp =
              if(StringUtils.IS_CARIPLO(tableName)) {
                if(approximationTimestamp.nonEmpty)
                  roundedTimestamp(format.parse(doubletString(2)),
                    approximationTimestamp.get)
                else doubletString(2).toLong
              } else instantToTimestamp(Integer.parseInt(doubletString(2)),
                approximationTimestamp)
          )
      }).toList, Map[String, String]()))
      .collect()
      .toMap
    (Dataset(mapToReturn), totalDoublets)
  }
}

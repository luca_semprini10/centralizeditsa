package parser

import java.io.{BufferedWriter, File, FileWriter}
import java.nio.file.{Path, Paths}

import com.opencsv.{CSVWriter, ICSVWriter}
import conf.MainConf
import model.{Dataset, Doublet}
import sparksql.HiveTableParser.{ARBITRARY_DATE, format}
import utils.StringUtils

import collection.JavaConverters._
import scala.util.{Failure, Success, Try}

/**
 * This object helps computing the output data of ITSA algorithm by writing
 * it into a n output CSV file.
 */
object CsvOutputWriter {

  private final val CSV_ATTRIBUTES_HEADER = List("userid", "trajectoryid", "attribute", "value")
  private final val CSV_HEADER = List("userid", "trajectoryid", "latitude", "longitude", "timestamp")
  private final val OUTPUT_PATH = s"outputs${File.separator}"

  /**
   * Method that parses the output dataset into a list of lists of strings, useful to be inserted
   * into a Csv file, which will do as an output result. If the dataset contains attribute values, then
   * returns another list of list of strings, useful to build another Csv file.
   * @param suppressed all the doublet suppressed by ITSA algorithm from the dataset.
   * @param originalTable the original input dataset.
   * @param datasetName the name of the input dataset.
   * @param samplingUsed the option value of time sampling, considered 1 if not present.
   * @return pair of rows of the output file, in form of list of lists of strings
   */
  def parseOutputDataset(suppressed: Set[Doublet],
                         originalTable: Dataset,
                         datasetName: String,
                         samplingUsed: Option[Int]): (List[List[String]], Option[List[List[String]]]) = {
    val filteredDataset = originalTable.dataset
      .map(it => it._1 -> (it._2._1
        .filter(d => suppressed.contains(d)), it._2._2))
      .filter(entry => entry._2._1.nonEmpty)
    (
      filteredDataset.flatMap(entry => entry._2._1.map(doublet => {
        val sample = samplingUsed.getOrElse(1)
        val initialDate = format.parse(ARBITRARY_DATE)
        List(
          if (StringUtils.IS_CARIPLO(datasetName)) entry._1.split(StringUtils.IDS_SEPARATOR)(0) else entry._1,
          if (StringUtils.IS_CARIPLO(datasetName)) entry._1.split(StringUtils.IDS_SEPARATOR)(1) else entry._1,
          doublet.latitude.toString,
          doublet.longitude.toString,
          if (StringUtils.IS_CARIPLO(datasetName)) doublet.timestamp.toString
          else ((doublet.timestamp - initialDate.getTime) / (1000.0 * 60 * sample)).toInt.toString)
      })).toList,
      if (StringUtils.IS_METRO_LIKE(datasetName))
        Some(parseOutputAttributes(Dataset(filteredDataset), datasetName))
      else None
    )
  }

  /**
   * Method that calls the writeCsvFile method and checks the result.
   * @param params the execution parameters, useful to generate the file name.
   * @param totalRows the actual number of rows computed by the algorithm.
   * @param distortionRatio final distortion ratio of the algorithm.
   * @param isDataset simple boolean value which is true if the file to write is relative
   *                  to the dataset, and false if it is relative to the attribute values.
   * @param rows the values of all the file rows.
   */
  def callWriteFunction(params: MainConf, totalRows: Int,
                        distortionRatio: Double,
                        isDataset: Boolean, rows: List[List[String]]): Unit = {
    writeCsvFile(params, totalRows, distortionRatio, isDataset = isDataset, rows) match {
      case Success(_) => println("!!! Done generating the output csv file !!!")
      case Failure(exception) =>
        println(s"ERROR GENERATING FILE: $exception")
        throw exception
    }
  }

  /**
   * Method that parses the output dataset in order to create the output csv file containing dataset attributes.
   * @param filteredDataset the output dataset filtered.
   * @param datasetName the name of the dataset.
   * @return the rows of the output file, in form of list of lists of strings.
   */
  private def parseOutputAttributes(filteredDataset: Dataset, datasetName: String): List[List[String]] = {
    filteredDataset.dataset.flatMap(entry => entry._2._2.map(attribute => {
      List(
        if (StringUtils.IS_CARIPLO(datasetName)) entry._1.split(StringUtils.IDS_SEPARATOR)(0) else entry._1,
        if (StringUtils.IS_CARIPLO(datasetName)) entry._1.split(StringUtils.IDS_SEPARATOR)(1) else entry._1,
        attribute._1,
        attribute._2)
    })).toList
  }

  /**
   * Method that writes a CSV file into the "outputs/" folder in the project's root folder, containing
   * the output of the ITSA algorithm.
   * @param params the execution parameters, useful to generate the file name.
   * @param totalRows the actual number of rows computed by the algorithm.
   * @param distortionRatio final distortion ratio of the algorithm.
   * @param rows the values of all the file rows.
   * @return a Try object, which success must be verified by caller.
   */
  private def writeCsvFile(params: MainConf, totalRows: Int,
                   distortionRatio: Double,
                   isDataset: Boolean, rows: List[List[String]]): Try[Unit] = {

    val currentRelativePath: Path = Paths.get("")
    val absolutePath: String = currentRelativePath.toAbsolutePath.toString + File.separator + OUTPUT_PATH
    val file = new File(absolutePath)
    file.mkdirs()
    Try (new CSVWriter(new BufferedWriter(new FileWriter(file.getAbsolutePath + File.separator +
      this.generateOutputFileName(params, params.num_rows.getOrElse(totalRows),
        distortionRatio, isDataset = isDataset))),
      ';',
      ICSVWriter.NO_QUOTE_CHARACTER,
      ICSVWriter.DEFAULT_ESCAPE_CHARACTER,
      ICSVWriter.DEFAULT_LINE_END))
      .flatMap((csvWriter: CSVWriter) =>
        Try {
          csvWriter.writeAll((
            (if (isDataset) CSV_HEADER else CSV_ATTRIBUTES_HEADER) +: rows).map(_.toArray).asJava)
          csvWriter.close()
        } match {
          case f @ Failure(_) => Try(csvWriter.close()).recoverWith {
            case _ => f
          }
          case success => success
        })
  }

  /**
   * Method that generates a file name for the csv file that I want to write.
   * @param params the execution parameters, useful to generate the file name.
   * @param totalRows the actual number of rows computed by the algorithm.
   * @param distortionRatio final distortion ratio of the algorithm.
   * @param isDataset simple boolean value which is true if the file to write is relative
   *                  to the dataset, and false if it is relative to the attribute values.
   * @return a file name, in order to the configuration parameters.
   */
  private def generateOutputFileName(params: MainConf, totalRows: Int,
                                     distortionRatio: Double, isDataset: Boolean): String = {
    s"ITSA-${if (isDataset) "DATASET" else "ATTRIBUTES"}-" +
      s"L_${params.threshold_l()}-K_${params.threshold_k()}-C_${params.threshold_c()}-" +
      s"N_${params.window_size_n()}-stepSize_${params.step_size()}-" +
      s"dataset_${params.dataset()}-rows_$totalRows-distRatio_$distortionRatio%.csv"
  }
}

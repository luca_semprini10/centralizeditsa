package parser

import java.io.InputStream
import java.util.Calendar

import model.{Dataset, DatasetRecord, Doublet}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.{BufferedSource, Source}

object TextParser {

  private final val METRO_LIKE_PATH = "datasets/metrolike/"
  private final val TEXT_PATH = s"${METRO_LIKE_PATH}text/"
  private final val CONFIG_PATH = s"${METRO_LIKE_PATH}config/"
  private val dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
  private val arbitraryDate = "2020-01-01 00:00:00"
  var totalDoublets = 0

  /**
   * Method to transform the integer time in the metro-like dataset into a Long date.
   * @param timeInInt the time integer present in the metro-like dataset.
   * @return a long, representing a custom date.
   */
  private def dateToTime(timeInInt: Int): Long = {
    val c = Calendar.getInstance
    c.setTime(dateFormat.parse(arbitraryDate))
    c.add(Calendar.MINUTE, timeInInt)
    c.getTime.getTime
  }

  /**
   * Method that returns a File, given a file name and an extension.
   * @param fileName the name of the file.
   * @param extension the file extension.
   * @return an InputStream object.
   */
  private def createFile(fileName: String, extension: String): InputStream = extension match {
    case ".txt" => this.getClass.getClassLoader.getResourceAsStream(TEXT_PATH + fileName + extension)
    case ".cfg" => this.getClass.getClassLoader.getResourceAsStream(CONFIG_PATH + fileName + extension)
    case _ => throw new IllegalArgumentException("Wrong file extension specified!!")
  }

  /**
   * Method to obtain the list of lines of a File.
   * @param filename the name of the File.
   * @return a list of lines.
   */
  private def getListOfLines(filename: String): List[String] = {
    val f = createFile(filename, ".txt")
    val bufferedSource: BufferedSource = Source.fromInputStream(f)
    val lines = bufferedSource.getLines().toList
    bufferedSource.close
    lines
  }

  /**
   * Method to split the line of the dataset and transform it into a Tuple2 containing a trajectory and a map of attributes.
   * @param line the line to transform.
   * @return a Tuple2 containing a trajectory (array of Strings) and a Map of attributes.
   */
  private def getTrajectoryAndMap(line: String): (Array[String], Map[String, String]) = {
    val trajectoryAndAttributes = line.split(":")
    val attributes = trajectoryAndAttributes(1).split(",")
    var mapOfAttributes = mutable.Map[String, String]()
    attributes.foreach(att => {
      mapOfAttributes += (attributes.indexOf(att).toString -> att)
    })
    (trajectoryAndAttributes(0).split(","), mapOfAttributes.toMap)
  }

  /**
   * Method to get the Map of sensitive values of a dataset.
   * @param filename the file name of the dataset.
   * @return a Map where keys are the attributes' positions and values are those attributes' values.
   */
  def getSensitiveValuesFromConfig(filename: String): Map[String, List[String]] = {
    val f = createFile(filename, ".cfg")
    val bufferedSource: BufferedSource = Source.fromInputStream(f)
    val lines = bufferedSource.getLines().toList
    bufferedSource.close
    Map("0" ->
      lines.last
        .split("\\{")
        .map(r => r.replace("}", ""))
        .filter(s => s.nonEmpty && s.contains("*"))
        .map(s => s.replace("*", ""))
        .map(s => s.replace(" ", ""))
        .toList
    )
  }

  /**
   * Method to get a list of DatasetRecords from a dataset file.
   * @param filename the name of the dataset file.
   * @return a list of DatasetRecords.
   */
  def textFileToListOfRecords(filename: String): List[DatasetRecord] = {
    this.totalDoublets = 0
    val listOfDatasetRecords: ListBuffer[DatasetRecord] = ListBuffer.empty
    val lines = getListOfLines(filename)
    lines.foreach(line => {
      val trajectoryAndMap = getTrajectoryAndMap(line)
      val listOfDoublet: ListBuffer[Doublet] = ListBuffer.empty
      trajectoryAndMap._1.foreach(doublet => {
        this.totalDoublets += 1
        val spaceAndTime = doublet.split("\\.")
        listOfDoublet += new Doublet(
          lines.indexOf(line).toString,
          Integer.parseInt(spaceAndTime(0).substring(1, spaceAndTime(0).length)).toDouble,
          Integer.parseInt(spaceAndTime(0).substring(1, spaceAndTime(0).length)).toDouble,
          dateToTime(Integer.parseInt(spaceAndTime(1).substring(1, spaceAndTime(1).length)))
        )
      })
      listOfDatasetRecords += new DatasetRecord(
        lines.indexOf(line),
        listOfDoublet.toList,
        trajectoryAndMap._2
      )
    })
    listOfDatasetRecords.toList
  }

  /**
   * Method to transform a list of strings into a Map representing the dataset.
   * @param lines the list of Strings.
   * @return a Map representing a dataset.
   */
  private def commonMapGeneration(lines: List[String],
                                  firstLines: Option[Int]): (Dataset, BigInt) = {
    this.totalDoublets = 0
    val mapOfDatasetRecords: mutable.Map[String, (List[Doublet], Map[String, String])] = mutable.Map.empty
    val toIterate = if (firstLines.isDefined) {
      if (firstLines.get > lines.size) lines
      else lines.take(firstLines.get)
    } else lines
    for((x,i) <- toIterate.view.zipWithIndex) {
      val trajectoryAndMap = getTrajectoryAndMap(x)
      val listOfDoublet: ListBuffer[Doublet] = ListBuffer.empty
      trajectoryAndMap._1.foreach(doublet => {
        this.totalDoublets += 1
        val spaceAndTime = doublet.split("\\.")
        listOfDoublet += new Doublet(
          i.toString,
          Integer.parseInt(spaceAndTime(0).substring(1, spaceAndTime(0).length)).toDouble,
          Integer.parseInt(spaceAndTime(0).substring(1, spaceAndTime(0).length)).toDouble,
          dateToTime(Integer.parseInt(spaceAndTime(1).substring(1, spaceAndTime(1).length)))
        )
      })
      mapOfDatasetRecords += (i.toString -> (listOfDoublet.toList, trajectoryAndMap._2))
    }
    (Dataset(mapOfDatasetRecords.toMap), totalDoublets)
  }
  /**
   * Method to return a raw representation of a dataset file.
   * @param filename the name of the dataset file.
   * @param firstRecords the number of first N records to take from the file, optional value
   * @return the dataset represented by a Map where keys are the trajectory ids, and the values are the Tuples,
   *         representing the trajectory, along with the map of attributes of the record, paired with the total
   *         number of doublets included.
   */
  def textFileToMapOfRecords(filename: String,
                             firstRecords: Option[Int]): (Dataset, BigInt) = {
    commonMapGeneration(getListOfLines(filename), firstRecords)
  }
}
